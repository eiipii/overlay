# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit git-r3
inherit cmake-utils

DESCRIPTION="A sparse LU factorization for square and rectangular matrices A, with Bartels-Golub-Reid updates for column replacement and other rank-1 modifications."
HOMEPAGE="https://web.stanford.edu/group/SOL/software/lusol/"
SRC_URI=""
EGIT_REPO_URI="https://github.com/paweld2/lusol_clone.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="sparse blas lapack"

RDEPEND=""
DEPEND="${RDEPEND}
        sparse? ( sci-libs/cxsparse )
        blas? ( virtual/blas )
        lapack? ( virtual/lapack )
        "



