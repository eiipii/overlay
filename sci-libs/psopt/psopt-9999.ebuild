# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

inherit cmake-utils autotools git-r3

CMAKE_MIN_VERSION=3.8

DESCRIPTION="PSOPT is an open source optimal control software package written in C++ that uses direct collocation methods"
HOMEPAGE="http://www.psopt.org/"
EGIT_REPO_URI="https://github.com/paweld2/psopt_cmake.git"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RDEPEND=">=sci-libs/ipopt-3.12.8
	     >=sci-libs/adolc-2.6.3
	     sci-libs/lusol
         virtual/blas
         virtual/lapack"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

