
gentoo-overlay
==============

Overlay for Gentoo/Linux packages for eiipii company.

## Warning

This overlay is not official. 

**Use ebuilds supplied in this repository on your own risk**. They've been tested on my own system setup (~amd64) and (most likely) tested on virtual systems (amd64 and x86).

## How to use.

To add the overlay to gentoo execute
    layman -o https://bitbucket.org/eiipii/overlay/raw/master/overlay.xml -f -a eiipii-overlay

